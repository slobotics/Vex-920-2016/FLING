# FLING
The code behind Michael, Ben B, and Aidan's bot.

## License
This project is licensed under the GNU GPLv3 license. See [LICENSE](LICENSE).
